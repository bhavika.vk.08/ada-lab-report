void sub(int n, int lis[],int mat[][100],int arr[])
{
    int i,j,k;
    for(i=0;i<n;i++)
    {
        mat[i][0]=arr[i];
    }
    for (i=0;i<n;i++)
    {
        lis[i]=1;
    }
    for (i=1;i<n;i++)
    {
        for (j=0;j<i;j++)
        {
            if (lis[j]>=lis[i] && arr[i]>arr[j])
            {
                lis[i]=lis[j]+1;
                for(k=0;k<lis[j];k++)
                {
                    mat[i][k]=mat[j][k];
                }
                mat[i][k]=arr[i];
            }
        }
    }
}

int findmax (int lis[],int n)
{
    int max=0,i;
    for (i=1;i<n;i++)
    {
        if (lis[i]>lis[max])
        {
            max=i;
        }
    }
    return max;
}

void disp (int mat[][100],int max,int lis[])
{
    int i,num;
    num=lis[max];
    printf("\n{\t");
    for (i=0;i<num;i++)
    {
        printf("%d\t",mat[max][i]);
    }
    printf("}\n");
}
