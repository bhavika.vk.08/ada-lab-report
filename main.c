#include<stdio.h>
#include <stdlib.h>
#include "func.h"

int main()
{
    int n,i,lis[100],mat[100][100],arr[100],max;
    printf("Enter the  length of a  sequence\n");
    scanf("%d",&n);
    printf("Enter %d elements of the sequence\n",n);
    for(i=0;i<n;i++)
        scanf("%d",&arr[i]);
    sub( n,lis,mat,arr);
    printf("The longest increasing subsequence is:\n");
    max=findmax (lis,n);
    disp ( mat,max,lis);
    printf("The length of the longest increasing  subsequence is : \n");
    printf("%d",lis[max]);

    return 0;
}